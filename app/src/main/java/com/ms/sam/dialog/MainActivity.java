package com.ms.sam.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button submit_button;
    Button red_button,blue_button,green_button;
    EditText et_text;
    String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        submit_button=findViewById(R.id.buttonID);

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                final Dialog dialog=new Dialog(MainActivity.this);

                dialog.setContentView(R.layout.firstview);

                dialog.setCancelable(false);

                red_button=dialog.findViewById(R.id.redID);
                green_button=dialog.findViewById(R.id.greenID);
                blue_button=dialog.findViewById(R.id.blueID);
                et_text=dialog.findViewById(R.id.edit_text);

                red_button.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onClick(View view)
                    {
                        text=et_text.getText().toString();

                        submit_button.setText(text);

                      //  submit_button.setBackgroundColor(Color.parseColor("#e7081b"));
                      //  it is used for changing the background colour of button

                        submit_button.setBackground(getDrawable(R.drawable.thirdcomponent));
                        dialog.dismiss();

                    }
                });

                blue_button.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onClick(View view) {

                        text=et_text.getText().toString();
                        submit_button.setText(text);

                        submit_button.setBackground(getDrawable(R.drawable.secondcomponent));
                        dialog.dismiss();


                    }
                });

                green_button.setOnClickListener(new View.OnClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onClick(View view)
                    {
//                        Log.d("Tab","Minakshi");
//
                text=et_text.getText().toString();

                        submit_button.setText(text);

                        submit_button.setBackground(getDrawable(R.drawable.firstcomponent));
                        dialog.dismiss();




                    }
                });



                dialog.show();
            }
        });

    }
}
